const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const commitmentSchema = mongoose.Schema({
  _id: ObjectId,
  created_at: Date,
  completed: Boolean,
  due_date: Date,
  minutes: Number,
  user_id: Number,
  user_name: String
});

module.exports = { commitment: commitmentSchema };