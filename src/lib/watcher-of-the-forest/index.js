var request = require("request")
var moment = require("moment-timezone")

var slack = require("./slack")
var commitments = require("./commitments")

var { abort, print, } = require("./util")

var config,
    lastStatuses = []

const startForestWatcher = () => {
    var watchFriends = config.WATCH_FRIENDS_IDS || []
    var requestsInterval = (config.REQUEST_INTERVAL || 10) * 1000

    const execWatcher = () => {
        const options = generatePayload()

        request(options, (error, response, body) => {
            if (error) {
                print('While trying to fetch Forest API', 'error')
                return console.log(error)
            }

            if (response.statusCode < 200 || response.statusCode >= 300) {
                print(`Status code ${response.statusCode} in Forest response`, 'error')
                throw new Error(response.statusCode)
            }

            const responseObj = JSON.parse(body)
            var statusMapping = []
            var totalChanges = 0

            const pickedFriendsStatuses = !watchFriends.length ? responseObj:
            responseObj.filter(status => watchFriends.indexOf(status.user_id) !== -1 )

            pickedFriendsStatuses.forEach(status => {
                statusMapping[status.user_id] = status
            })

            if (!lastStatuses.length) {
                lastStatuses = statusMapping
            }

            statusMapping.forEach((v, k) => {
                const last = lastStatuses[k]

                if (v.total_minute > last.total_minute) {
                    totalChanges++
                    print('notify updater', 'info')
                    notifyUpdater(last, v)
                }
            })

            if (totalChanges)
                print(`${totalChanges} changes at ${Date.now()}`)

            lastStatuses = statusMapping
            /// DEV ONLY
            if (config.environment === "dev" && config.FOREST_FORCE_UPDATE) {
                lastStatuses = lastStatuses.map(e => ({
                    ...e,
                    total_minute: e.total_minute - 1
                }))
            }
        })
    }

    execWatcher()
    setInterval(execWatcher, requestsInterval)
}

const start = (setup) => {
    if (!setup) throw abort(print('No setup information provided.', 'abort'))
    config = setup

    console.log("🌲🌲🌲 Forest Service 🌲🌲🌲")
    startForestWatcher()
}

module.exports = { start, commitments }

function notifyUpdater(oldStatus, newStatus) {
    const didSucceed = didSucceedForestSession(oldStatus, newStatus)
    const msg = slack.composeMessageNotification(newStatus, oldStatus, didSucceed)

    slack.postSlackMessage(config.SLACK_CALLBACK_URL, msg, slackResponse => {
        console.log({ slackResponse })
        if (slackResponse.error) {
            print(`Posting the message on Slack: ${slackResponse.err}`, 'error')
        }
    })
}

function didSucceedForestSession (oldStatus, newStatus) {
    if (newStatus.dead_count > oldStatus.dead_count) {
        return false
    } else if (newStatus.health_count > oldStatus.health_count) {
        return true
    }

    // This scenario should never happen. Something is wrong there.
    print(`Living and dead trees keep the same ammount`, 'warning')
    console.log(oldStatus, newStatus)
    return true
}

var generatePayload = () => {
    // @TODO: Change this horrible way of using moment and formating dates
    let today = moment().tz('America/Mexico_City')
    let tomorrow = moment().tz('America/Mexico_City').add(1, 'day')

    const initDate = `${today.get('year')}-${today.get('month') + 1}-${today.get('date') }`
    const endDate = `${tomorrow.get('year')}-${tomorrow.get('month') + 1}-${tomorrow.get('date')}`

    return {
        method: 'GET',
        url: 'https://c88fef96.forestapp.cc/api/v1/friends/leaderboards',
        qs:
        {
            brief: 'true',
            from_date: `${initDate}T06:00:00.000Z`,
            to_date: `${endDate}T06:00:00.000Z`
        },
        headers:
        {
            Cookie: `remember_token=${config.FOREST_REMEMBER_TOKEN}`,
        }
    }
}