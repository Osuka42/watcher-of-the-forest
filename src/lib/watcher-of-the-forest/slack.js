const request = require("request");

module.exports = {
    postSlackMessage: (callbackUrl, msg, callback) => {
        var options = {
            method: 'POST',
            url: callbackUrl,
            headers:
            {
                'cache-control': 'no-cache',
                'Content-type': 'application/json'
            },
            body: `{"text":"${msg}"}`
        };

        request(options, function (error, response, body) {
            callback({ error, response, body })
        });
    },

    // @TODO: Change this for a clever way, without using boolean params.
    composeMessageNotification: (newStatus, oldStatus, didSucceed) => {
        if (didSucceed)
            return `*${newStatus.name}* just completed ${newStatus.total_minute - oldStatus.total_minute} minutes, for *${newStatus.total_minute} total today*! Keep it up! 🌲`;
        return `Oh no *${newStatus.name}!* Completed ${newStatus.total_minute - oldStatus.total_minute} only minutes, you can do it better next time!`;
    }
}