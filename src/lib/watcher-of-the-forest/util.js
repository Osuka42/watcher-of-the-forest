
module.exports = {
    print: (msg, type = 'info') => {
        const prefix = '🌲';
        // I think there is already a standard for logging codes..?
        const types = {
            abort: 'Abort',
            error: 'Error',
            warning: 'Warning',
            verbose: 'Verbose',
            info: 'Info',
        }
        const text = `${prefix} ${types[type]}: ${msg}`;
        console.log(text);
        return text;
    },

    abort: abortMsg => {
    // Logs anything you need to log before crashing.
    return abortMsg;
    }
}