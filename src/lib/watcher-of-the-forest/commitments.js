const mongoose = require('mongoose');
const config = require('../../../config');
const commitment = require("./models").commitment;

// const config = require();

// mongoose.connect(config.db.MONGO_URI);

module.exports = {
    getAll: async () => {
        try {
            const result = await commitment.find();
            console.log(result);
            res.send(result);
        } catch (err) {
            console.log(err);
        }
        res.send({ no: "none" });
    },

    getActiveCommitments: async () => {
        return [{}]
    },

    getCompletedCommitments: async () => {
        return [{}, {}]
    }
}