# Watcher of the Forest

### Requeriments
- Node v10.x


### Step 0: Config your env
Example
```
FOREST_REMEMBER_TOKEN=2befd94d1ee89593b78a77cf826d39f037f2f991
FOREST_WATCH_FRIENDS_IDS=100000,200000,300000,400000,999999
FOREST_REQUEST_INTERVAL=10

SLACK_CALLBACK_URL=https://hooks.slack.com/services/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
```

**FOREST_REMEMBER_TOKEN**: Login into slack with your phone and extract that value from requests.

**FOREST_WATCH_FRIENDS_IDS** empty to track ALL your friends.



### Installation
```
npm install
npm run start
```

forestService(config.forest);