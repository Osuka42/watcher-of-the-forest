var config = require('./config')
var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
// const mongoose = require('mongoose')

var forestService = require('./src/lib/watcher-of-the-forest')
var apiRoute = require('./routes/api')

var app = express()

// try {
//   console.log('config.db.MONGO_URI', config.db.MONGO_URI)
//   mongoose.connect( config.db.MONGO_URI, { useNewUrlParser: true } )
//     .then((err) => {
//         if (err) {
//           console.log("Error conecting!")
//           throw err
//         }
//         console.log("Connected!")
//       }
//     ).catch(err => {
//       console.log(err)
//     })
// } catch(e) {
//   console.log({e})
// }

// mongoose.connection
//   .on('error', error => console.log(error))
//   .once('open', () => { console.log('Success connecting to mongodb') })

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use(apiRoute)

var forest = forestService.start(config.forest)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
