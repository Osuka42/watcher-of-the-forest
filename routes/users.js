var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

/* Base for commitments */
router.get('/commitments', function(req, res, next) {
  res.render('index', { title: 'Create new commitment' });
});

module.exports = router;
