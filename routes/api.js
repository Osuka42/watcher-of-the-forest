var express = require('express');
var router = express.Router();

var commitments = require('../src/lib/watcher-of-the-forest').commitments;

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', { title: 'Express', body: 'Hello, this is probably not what you are looking for.' });
});

// Create commitment
// Todo
router.post('/api/commitments', (req, res, next) => {
    res.send(req.body);
});

// List commitments of the day
// Todo
router.get('/api/commitments', async (req, res, next) => {
    var todayCommitments = await commitments.getAll();
    res.send(todayCommitments);
});


module.exports = router;
