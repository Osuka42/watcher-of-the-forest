// This might contain sentive information, watch out when pushing to git
require('dotenv').config()

const SLACK_CALLBACK_URL = process.env.SLACK_CALLBACK_URL || ""
const environment = process.env.ENV

module.exports = {
  environment: environment,
  slack: {
    SLACK_CALLBACK_URL
  },
  forest: {
    REQUEST_INTERVAL: process.env.FOREST_REQUEST_INTERVAL || 10,
    WATCH_FRIENDS_IDS: (process.env.FOREST_WATCH_FRIENDS_IDS || "")
      .split(",")
      .map(e => parseInt(e, 10)),
    SLACK_CALLBACK_URL,
    FOREST_REMEMBER_TOKEN: process.env.FOREST_REMEMBER_TOKEN || null,
    environment: environment,

    // Development mode only !!!!!!
    FOREST_FORCE_UPDATE: process.env.FOREST_FORCE_UPDATE || false
  },
  db: {
    MONGO_URI: process.env.MONGO_CONNECT
  }
};